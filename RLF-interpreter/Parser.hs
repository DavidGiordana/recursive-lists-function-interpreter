{-# OPTIONS_GHC -w #-}
module Parser where

import Failable

import Common
import Data.Maybe
import Data.Char

-- parser produced by Happy Version 1.19.0

data HappyAbsSyn t6 t7 t8 t9 t10 t11 t12 t13 t14
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8
	| HappyAbsSyn9 t9
	| HappyAbsSyn10 t10
	| HappyAbsSyn11 t11
	| HappyAbsSyn12 t12
	| HappyAbsSyn13 t13
	| HappyAbsSyn14 t14

action_0 (26) = happyShift action_5
action_0 (27) = happyShift action_6
action_0 (6) = happyGoto action_12
action_0 _ = happyFail

action_1 (19) = happyShift action_10
action_1 (27) = happyShift action_11
action_1 (13) = happyGoto action_9
action_1 _ = happyFail

action_2 (21) = happyShift action_8
action_2 (9) = happyGoto action_7
action_2 _ = happyFail

action_3 (26) = happyShift action_5
action_3 (27) = happyShift action_6
action_3 (6) = happyGoto action_4
action_3 _ = happyFail

action_4 (18) = happyShift action_13
action_4 _ = happyFail

action_5 (27) = happyShift action_30
action_5 _ = happyFail

action_6 (15) = happyShift action_29
action_6 _ = happyFail

action_7 (36) = happyAccept
action_7 _ = happyFail

action_8 (21) = happyShift action_8
action_8 (23) = happyShift action_21
action_8 (27) = happyShift action_22
action_8 (30) = happyShift action_23
action_8 (31) = happyShift action_24
action_8 (32) = happyShift action_25
action_8 (33) = happyShift action_26
action_8 (34) = happyShift action_27
action_8 (35) = happyShift action_28
action_8 (9) = happyGoto action_17
action_8 (10) = happyGoto action_18
action_8 (11) = happyGoto action_19
action_8 (12) = happyGoto action_20
action_8 _ = happyReduce_13

action_9 (17) = happyShift action_16
action_9 (36) = happyAccept
action_9 _ = happyFail

action_10 (25) = happyShift action_15
action_10 (14) = happyGoto action_14
action_10 _ = happyReduce_28

action_11 _ = happyReduce_25

action_12 (18) = happyShift action_13
action_12 (36) = happyAccept
action_12 _ = happyFail

action_13 (26) = happyShift action_5
action_13 (27) = happyShift action_6
action_13 (6) = happyGoto action_41
action_13 _ = happyFail

action_14 (20) = happyShift action_40
action_14 _ = happyFail

action_15 (25) = happyShift action_15
action_15 (14) = happyGoto action_39
action_15 _ = happyReduce_28

action_16 (19) = happyShift action_10
action_16 (27) = happyShift action_11
action_16 (13) = happyGoto action_38
action_16 _ = happyFail

action_17 _ = happyReduce_16

action_18 (22) = happyShift action_37
action_18 _ = happyFail

action_19 (21) = happyShift action_8
action_19 (23) = happyShift action_21
action_19 (27) = happyShift action_22
action_19 (30) = happyShift action_23
action_19 (31) = happyShift action_24
action_19 (32) = happyShift action_25
action_19 (33) = happyShift action_26
action_19 (34) = happyShift action_27
action_19 (35) = happyShift action_28
action_19 (9) = happyGoto action_17
action_19 (10) = happyGoto action_36
action_19 (11) = happyGoto action_19
action_19 (12) = happyGoto action_20
action_19 _ = happyReduce_13

action_20 _ = happyReduce_14

action_21 (21) = happyShift action_8
action_21 (23) = happyShift action_21
action_21 (27) = happyShift action_22
action_21 (30) = happyShift action_23
action_21 (31) = happyShift action_24
action_21 (32) = happyShift action_25
action_21 (33) = happyShift action_26
action_21 (34) = happyShift action_27
action_21 (35) = happyShift action_28
action_21 (9) = happyGoto action_17
action_21 (10) = happyGoto action_35
action_21 (11) = happyGoto action_19
action_21 (12) = happyGoto action_20
action_21 _ = happyReduce_13

action_22 _ = happyReduce_17

action_23 _ = happyReduce_18

action_24 _ = happyReduce_19

action_25 _ = happyReduce_20

action_26 _ = happyReduce_21

action_27 _ = happyReduce_22

action_28 _ = happyReduce_23

action_29 (19) = happyShift action_10
action_29 (21) = happyShift action_8
action_29 (27) = happyShift action_11
action_29 (7) = happyGoto action_32
action_29 (9) = happyGoto action_33
action_29 (13) = happyGoto action_34
action_29 _ = happyFail

action_30 (16) = happyShift action_31
action_30 _ = happyFail

action_31 (28) = happyShift action_45
action_31 (29) = happyShift action_46
action_31 (8) = happyGoto action_44
action_31 _ = happyFail

action_32 _ = happyReduce_5

action_33 (19) = happyShift action_10
action_33 (27) = happyShift action_11
action_33 (13) = happyGoto action_43
action_33 _ = happyReduce_6

action_34 (17) = happyShift action_16
action_34 _ = happyReduce_7

action_35 (24) = happyShift action_42
action_35 _ = happyFail

action_36 _ = happyReduce_12

action_37 _ = happyReduce_11

action_38 (17) = happyShift action_16
action_38 _ = happyReduce_26

action_39 _ = happyReduce_27

action_40 _ = happyReduce_24

action_41 (18) = happyShift action_13
action_41 _ = happyReduce_3

action_42 _ = happyReduce_15

action_43 (17) = happyShift action_16
action_43 _ = happyReduce_8

action_44 (15) = happyShift action_47
action_44 _ = happyFail

action_45 _ = happyReduce_9

action_46 _ = happyReduce_10

action_47 (19) = happyShift action_10
action_47 (21) = happyShift action_8
action_47 (27) = happyShift action_11
action_47 (7) = happyGoto action_48
action_47 (9) = happyGoto action_33
action_47 (13) = happyGoto action_34
action_47 _ = happyFail

action_48 _ = happyReduce_4

happyReduce_3 = happySpecReduce_3  6 happyReduction_3
happyReduction_3 (HappyAbsSyn6  happy_var_3)
	_
	(HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn6
		 (Seq happy_var_1 happy_var_3
	)
happyReduction_3 _ _ _  = notHappyAtAll 

happyReduce_4 = happyReduce 6 6 happyReduction_4
happyReduction_4 ((HappyAbsSyn7  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TVar happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn6
		 (VarDef happy_var_2 happy_var_4 happy_var_6
	) `HappyStk` happyRest

happyReduce_5 = happySpecReduce_3  6 happyReduction_5
happyReduction_5 (HappyAbsSyn7  happy_var_3)
	_
	(HappyTerminal (TVar happy_var_1))
	 =  HappyAbsSyn6
		 (Assign happy_var_1 happy_var_3
	)
happyReduction_5 _ _ _  = notHappyAtAll 

happyReduce_6 = happySpecReduce_1  7 happyReduction_6
happyReduction_6 (HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn7
		 (Func happy_var_1
	)
happyReduction_6 _  = notHappyAtAll 

happyReduce_7 = happySpecReduce_1  7 happyReduction_7
happyReduction_7 (HappyAbsSyn13  happy_var_1)
	 =  HappyAbsSyn7
		 (List happy_var_1
	)
happyReduction_7 _  = notHappyAtAll 

happyReduce_8 = happySpecReduce_2  7 happyReduction_8
happyReduction_8 (HappyAbsSyn13  happy_var_2)
	(HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn7
		 (Apply happy_var_1 happy_var_2
	)
happyReduction_8 _ _  = notHappyAtAll 

happyReduce_9 = happySpecReduce_1  8 happyReduction_9
happyReduction_9 _
	 =  HappyAbsSyn8
		 (VTList
	)

happyReduce_10 = happySpecReduce_1  8 happyReduction_10
happyReduction_10 _
	 =  HappyAbsSyn8
		 (VTFunc
	)

happyReduce_11 = happySpecReduce_3  9 happyReduction_11
happyReduction_11 _
	(HappyAbsSyn10  happy_var_2)
	_
	 =  HappyAbsSyn9
		 (happy_var_2
	)
happyReduction_11 _ _ _  = notHappyAtAll 

happyReduce_12 = happySpecReduce_2  10 happyReduction_12
happyReduction_12 (HappyAbsSyn10  happy_var_2)
	(HappyAbsSyn11  happy_var_1)
	 =  HappyAbsSyn10
		 (Comp happy_var_1 happy_var_2
	)
happyReduction_12 _ _  = notHappyAtAll 

happyReduce_13 = happySpecReduce_0  10 happyReduction_13
happyReduction_13  =  HappyAbsSyn10
		 (None
	)

happyReduce_14 = happySpecReduce_1  11 happyReduction_14
happyReduction_14 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn11
		 (happy_var_1
	)
happyReduction_14 _  = notHappyAtAll 

happyReduce_15 = happySpecReduce_3  11 happyReduction_15
happyReduction_15 _
	(HappyAbsSyn10  happy_var_2)
	_
	 =  HappyAbsSyn11
		 (Rep happy_var_2
	)
happyReduction_15 _ _ _  = notHappyAtAll 

happyReduce_16 = happySpecReduce_1  11 happyReduction_16
happyReduction_16 (HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn11
		 (happy_var_1
	)
happyReduction_16 _  = notHappyAtAll 

happyReduce_17 = happySpecReduce_1  11 happyReduction_17
happyReduction_17 (HappyTerminal (TVar happy_var_1))
	 =  HappyAbsSyn11
		 (FVar happy_var_1
	)
happyReduction_17 _  = notHappyAtAll 

happyReduce_18 = happySpecReduce_1  12 happyReduction_18
happyReduction_18 _
	 =  HappyAbsSyn12
		 (Or
	)

happyReduce_19 = happySpecReduce_1  12 happyReduction_19
happyReduction_19 _
	 =  HappyAbsSyn12
		 (Ol
	)

happyReduce_20 = happySpecReduce_1  12 happyReduction_20
happyReduction_20 _
	 =  HappyAbsSyn12
		 (Rr
	)

happyReduce_21 = happySpecReduce_1  12 happyReduction_21
happyReduction_21 _
	 =  HappyAbsSyn12
		 (Rl
	)

happyReduce_22 = happySpecReduce_1  12 happyReduction_22
happyReduction_22 _
	 =  HappyAbsSyn12
		 (Sr
	)

happyReduce_23 = happySpecReduce_1  12 happyReduction_23
happyReduction_23 _
	 =  HappyAbsSyn12
		 (Sl
	)

happyReduce_24 = happySpecReduce_3  13 happyReduction_24
happyReduction_24 _
	(HappyAbsSyn14  happy_var_2)
	_
	 =  HappyAbsSyn13
		 (L happy_var_2
	)
happyReduction_24 _ _ _  = notHappyAtAll 

happyReduce_25 = happySpecReduce_1  13 happyReduction_25
happyReduction_25 (HappyTerminal (TVar happy_var_1))
	 =  HappyAbsSyn13
		 (LVar happy_var_1
	)
happyReduction_25 _  = notHappyAtAll 

happyReduce_26 = happySpecReduce_3  13 happyReduction_26
happyReduction_26 (HappyAbsSyn13  happy_var_3)
	_
	(HappyAbsSyn13  happy_var_1)
	 =  HappyAbsSyn13
		 (Cat happy_var_1 happy_var_3
	)
happyReduction_26 _ _ _  = notHappyAtAll 

happyReduce_27 = happySpecReduce_2  14 happyReduction_27
happyReduction_27 (HappyAbsSyn14  happy_var_2)
	(HappyTerminal (TInt happy_var_1))
	 =  HappyAbsSyn14
		 (happy_var_1 : happy_var_2
	)
happyReduction_27 _ _  = notHappyAtAll 

happyReduce_28 = happySpecReduce_0  14 happyReduction_28
happyReduction_28  =  HappyAbsSyn14
		 ([]
	)

happyNewToken action sts stk
	= lexer(\tk -> 
	let cont i = action i i tk (HappyState action) sts stk in
	case tk of {
	TEOF -> action 36 36 tk (HappyState action) sts stk;
	TEquals -> cont 15;
	TColon -> cont 16;
	TDot -> cont 17;
	TSemiColon -> cont 18;
	TBOpen -> cont 19;
	TBClose -> cont 20;
	TBrOpen -> cont 21;
	TBrClose -> cont 22;
	Tlt -> cont 23;
	Tgt -> cont 24;
	TInt happy_dollar_dollar -> cont 25;
	TVarDef -> cont 26;
	TVar happy_dollar_dollar -> cont 27;
	TVTList -> cont 28;
	TVTFunc -> cont 29;
	TOR -> cont 30;
	TOL -> cont 31;
	TRR -> cont 32;
	TRL -> cont 33;
	TSR -> cont 34;
	TSL -> cont 35;
	_ -> happyError' tk
	})

happyError_ 36 tk = happyError' tk
happyError_ _ tk = happyError' tk

happyThen :: () => P a -> (a -> P b) -> P b
happyThen = (thenP)
happyReturn :: () => a -> P a
happyReturn = (returnP)
happyThen1 = happyThen
happyReturn1 :: () => a -> P a
happyReturn1 = happyReturn
happyError' :: () => (Token) -> P a
happyError' tk = (\token -> happyError) tk

parse_Comm = happySomeParser where
  happySomeParser = happyThen (happyParse action_0) (\x -> case x of {HappyAbsSyn6 z -> happyReturn z; _other -> notHappyAtAll })

parseList = happySomeParser where
  happySomeParser = happyThen (happyParse action_1) (\x -> case x of {HappyAbsSyn13 z -> happyReturn z; _other -> notHappyAtAll })

parseFunc = happySomeParser where
  happySomeParser = happyThen (happyParse action_2) (\x -> case x of {HappyAbsSyn9 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


type LineNumber = Int
type P a = String -> LineNumber -> Failable a

getLineNo :: P LineNumber
getLineNo = \s l -> Ok l

thenP :: P a -> (a -> P b) -> P b
m `thenP` k = \s l-> case m s l of
                         Ok a     -> k a s l
                         Error e -> Error e

returnP :: a -> P a
returnP a = \s l-> Ok a

failP :: String -> P a
failP err = \s l -> Error err

catchP :: P a -> (String -> P a) -> P a
catchP m k = \s l -> case m s l of
                        Ok a     -> Ok a
                        Error e -> k e s l

happyError :: P a
happyError = \ _ i -> Error $ "Error de parseo -> Linea " ++ (show (i::LineNumber))


data Token  = TEquals
            | TColon
            | TDot
            | TSemiColon
            | TBOpen
            | TBClose
            | TBrOpen
            | TBrClose
            | Tlt
            | Tgt
            | TInt Int
            -- Comandos
            | TVarDef
            | TVar String
            | TVTList
            | TVTFunc

            | TOL
            | TOR
            | TRL
            | TRR
            | TSL
            | TSR
            | TEOF
            deriving Show

----------------------------------

readNumber :: String -> (Int, String)
readNumber xs = readNumber' xs 0

readNumber' :: String -> Int -> (Int, String)
readNumber' [] n = (n, [])
readNumber' l@(x:xs) n = if isDigit x then readNumber' xs (n*10 + (digitToInt x))
                            else (n, l)

lexer cont s = case s of
                   [] -> cont TEOF []
                   ('\n':s)  ->  \line -> lexer cont s (line + 1)
                   (c:cs)
                         | isSpace c -> lexer cont cs
                         | isAlphaNum c -> lexAlphaNum (c:cs)
                   -- Comentarios
                   ('-':('-':cs)) -> lexer cont $ dropWhile ((/=) '\n') cs
                   ('{':('-':cs)) -> consumirBK 0 0 cont cs
                   ('-':('}':cs)) -> \ line -> Error $ "Error de parseo: Linea " ++ (show line) ++ " -> Comentario no cerrado"
                   -- Simbolos
                   ('=':cs) -> cont TEquals cs
                   ('.':cs) -> cont TDot cs
                   (':':cs) -> cont TColon cs
                   (';':cs) -> cont TSemiColon cs
                   ('[':cs) -> cont TBOpen cs
                   (']':cs) -> cont TBClose cs
                   ('{':cs) -> cont TBrOpen cs
                   ('}':cs) -> cont TBrClose cs
                   ('<':cs) -> cont Tlt cs
                   ('>':cs) -> cont Tgt cs
                   unknown -> \line ->
                                       Error $ "Error de parseo: Línea " ++ (show line)
                                       ++ "-> No se puede reconocer "
                                       ++ (show $ take 10 unknown) ++ "..."
                   where lexAlphaNum cs@(x:_) = if isDigit x
                                                    then let (n,xs) = readNumber cs
                                                                    in cont (TInt n) xs
                                                    else
                                                        case span isAlphaNum cs of
                                                              ("var", rest)                   -> cont TVarDef rest
                                                              ("list", rest)                  -> cont TVTList rest
                                                              ("function", rest)              -> cont TVTFunc rest
                                                              ("func", rest)                  -> cont TVTFunc rest

                                                              ("or", rest)                    -> cont TOR rest
                                                              ("ol", rest)                    -> cont TOL rest
                                                              ("rr", rest)                    -> cont TRR rest
                                                              ("rl", rest)                    -> cont TRL rest
                                                              ("sr", rest)                    -> cont TSR rest
                                                              ("sl", rest)                    -> cont TSL rest

                                                              (var,rest)                      -> cont (TVar var) rest
                         consumirBK anidado cl cont s = case s of
                                                              ('-':('-':cs)) -> consumirBK anidado cl cont $ dropWhile ((/=) '\n') cs
                                                              ('{':('-':cs)) -> consumirBK (anidado+1) cl cont cs
                                                              ('-':('}':cs)) -> case anidado of
                                                                                 0 -> \line -> lexer cont cs (line+cl)
                                                                                 _ -> consumirBK (anidado-1) cl cont cs
                                                              ('\n':cs) -> consumirBK anidado (cl+1) cont cs
                                                              (_:cs) -> consumirBK anidado cl cont cs

list s = parseList s 1
func s = parseFunc s 1
parseComm s = parse_Comm s 1
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<command-line>" #-}





# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4














# 1 "/usr/include/x86_64-linux-gnu/bits/predefs.h" 1 3 4

# 18 "/usr/include/x86_64-linux-gnu/bits/predefs.h" 3 4












# 31 "/usr/include/stdc-predef.h" 2 3 4








# 5 "<command-line>" 2
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 45 "templates/GenericTemplate.hs" #-}








{-# LINE 66 "templates/GenericTemplate.hs" #-}

{-# LINE 76 "templates/GenericTemplate.hs" #-}

{-# LINE 85 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
	happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
	 (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 154 "templates/GenericTemplate.hs" #-}

-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
	 sts1@(((st1@(HappyState (action))):(_))) ->
        	let r = fn stk in  -- it doesn't hurt to always seq here...
       		happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 255 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--	trace "failing" $ 
        happyError_ i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
						(saved_tok `HappyStk` _ `HappyStk` stk) =
--	trace ("discarding state, depth " ++ show (length stk))  $
	action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
	action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--	happySeq = happyDoSeq
-- otherwise it emits
-- 	happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 321 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
