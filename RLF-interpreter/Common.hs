module Common where

-- Variable
type Variable = String

-- Tipos de variables (Para identificación)
data VT = VTFunc -- Funcion
        | VTList -- Lista
        deriving Show

-- Funciones Recursivas Primitivas
data RLFunc = Ol                    -- Insertar cero a la izquierda
            | Or                    -- Insertar cero a la derecha
            | Rl                    -- Elimina elemento a la izquierda
            | Rr                    -- Elimina elemento a la derecha
            | Sl                    -- Aplica el sucesor a la izquierda
            | Sr                    -- Aplica el sucesor a la derecha
            | Rep RLFunc            -- Repetición de función
            | FVar Variable         -- Referencia a variable
            | Comp RLFunc RLFunc    -- Composición de funciones
            | None                  -- Operación nula (funciona como la identidad)
            deriving Show

-- Listas
data Lists  = L [Int]               -- Lista de enteros
            | Cat Lists Lists       -- Concatenación de listas
            | LVar Variable         -- Referencia a variable
            deriving Show

-- Valor asignable en variable
data Value  = Func RLFunc           -- Funcion Recursiva Primitiva
            | List Lists            -- Lista
            | Apply RLFunc Lists    -- Aplicación de función en lista
            deriving Show

-- Comandos ejecutables
data Comm   = VarDef Variable VT Value  -- Definición de variable
            | Assign Variable Value     -- Asignación en variable
            | Seq Comm Comm             -- Secuencia de comandos ejecutables
            deriving Show
