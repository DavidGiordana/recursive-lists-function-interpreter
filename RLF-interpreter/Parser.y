{
module Parser where

import Failable

import Common
import Data.Maybe
import Data.Char

}

%monad { P } { thenP } { returnP }
%name parse_Comm Comm
%name parseList List
%name parseFunc Function

%tokentype { Token }
%lexer {lexer} {TEOF}


%token
    '='     			 { TEquals }
    ':'     			 { TColon }
    '.'                  { TDot }
    ';'                  { TSemiColon }
    '['                  { TBOpen }
    ']'                  { TBClose }
    '{'                  { TBrOpen }
    '}'                  { TBrClose }
    '<'                  { Tlt }
    '>'                  { Tgt }
    INT                  { TInt $$ }
    VARDEF               { TVarDef }
    VAR                  { TVar $$ }
    LIST                 { TVTList }
    FUNC                 { TVTFunc }
    OR                   { TOR }
    OL                   { TOL }
    RR                   { TRR }
    RL                   { TRL }
    SR                   { TSR }
    SL                   { TSL }


%right '.'
%right ';'
%%

Comm    : Comm ';' Comm                         { Seq $1 $3 }
        | VARDEF VAR ':' Type '=' ValueExp      { VarDef $2 $4 $6 }
        | VAR '=' ValueExp                      { Assign $1 $3 }

ValueExp    : Function                          { Func $1 }
            | List                              { List $1 }
            | Function List                     { Apply $1 $2 }

Type        : LIST                              { VTList }
            | FUNC                              { VTFunc }

Function     : '{' Comp_Func '}'                { $2 }

Comp_Func    : Func  Comp_Func                  { Comp $1 $2 }
             |                                  { None }

Func            : BaseFunctions                 { $1 }
                | '<' Comp_Func '>'             { Rep $2 }
                | Function                      { $1 }
                | VAR                           { FVar $1 }

BaseFunctions   : OR                            { Or }
                | OL                            { Ol }
                | RR                            { Rr }
                | RL                            { Rl }
                | SR                            { Sr }
                | SL                            { Sl }

List        : '[' List_Int ']'                  { L $2 }
            | VAR                               { LVar $1 }
            | List '.' List                     { Cat $1 $3 }

List_Int    : INT  List_Int                     { $1 : $2 }
            |                                   { [] }

{

type LineNumber = Int
type P a = String -> LineNumber -> Failable a

getLineNo :: P LineNumber
getLineNo = \s l -> Ok l

thenP :: P a -> (a -> P b) -> P b
m `thenP` k = \s l-> case m s l of
                         Ok a     -> k a s l
                         Error e -> Error e

returnP :: a -> P a
returnP a = \s l-> Ok a

failP :: String -> P a
failP err = \s l -> Error err

catchP :: P a -> (String -> P a) -> P a
catchP m k = \s l -> case m s l of
                        Ok a     -> Ok a
                        Error e -> k e s l

happyError :: P a
happyError = \ _ i -> Error $ "Error de parseo -> Linea " ++ (show (i::LineNumber))


data Token  = TEquals
            | TColon
            | TDot
            | TSemiColon
            | TBOpen
            | TBClose
            | TBrOpen
            | TBrClose
            | Tlt
            | Tgt
            | TInt Int
            -- Comandos
            | TVarDef
            | TVar String
            | TVTList
            | TVTFunc

            | TOL
            | TOR
            | TRL
            | TRR
            | TSL
            | TSR
            | TEOF
            deriving Show

----------------------------------

readNumber :: String -> (Int, String)
readNumber xs = readNumber' xs 0

readNumber' :: String -> Int -> (Int, String)
readNumber' [] n = (n, [])
readNumber' l@(x:xs) n = if isDigit x then readNumber' xs (n*10 + (digitToInt x))
                            else (n, l)

lexer cont s = case s of
                   [] -> cont TEOF []
                   ('\n':s)  ->  \line -> lexer cont s (line + 1)
                   (c:cs)
                         | isSpace c -> lexer cont cs
                         | isAlphaNum c -> lexAlphaNum (c:cs)
                   -- Comentarios
                   ('-':('-':cs)) -> lexer cont $ dropWhile ((/=) '\n') cs
                   ('{':('-':cs)) -> consumirBK 0 0 cont cs
                   ('-':('}':cs)) -> \ line -> Error $ "Error de parseo: Linea " ++ (show line) ++ " -> Comentario no cerrado"
                   -- Simbolos
                   ('=':cs) -> cont TEquals cs
                   ('.':cs) -> cont TDot cs
                   (':':cs) -> cont TColon cs
                   (';':cs) -> cont TSemiColon cs
                   ('[':cs) -> cont TBOpen cs
                   (']':cs) -> cont TBClose cs
                   ('{':cs) -> cont TBrOpen cs
                   ('}':cs) -> cont TBrClose cs
                   ('<':cs) -> cont Tlt cs
                   ('>':cs) -> cont Tgt cs
                   unknown -> \line ->
                                       Error $ "Error de parseo: Línea " ++ (show line)
                                       ++ "-> No se puede reconocer "
                                       ++ (show $ take 10 unknown) ++ "..."
                   where lexAlphaNum cs@(x:_) = if isDigit x
                                                    then let (n,xs) = readNumber cs
                                                                    in cont (TInt n) xs
                                                    else
                                                        case span isAlphaNum cs of
                                                              ("var", rest)                   -> cont TVarDef rest
                                                              ("list", rest)                  -> cont TVTList rest
                                                              ("function", rest)              -> cont TVTFunc rest
                                                              ("func", rest)                  -> cont TVTFunc rest

                                                              ("or", rest)                    -> cont TOR rest
                                                              ("ol", rest)                    -> cont TOL rest
                                                              ("rr", rest)                    -> cont TRR rest
                                                              ("rl", rest)                    -> cont TRL rest
                                                              ("sr", rest)                    -> cont TSR rest
                                                              ("sl", rest)                    -> cont TSL rest

                                                              (var,rest)                      -> cont (TVar var) rest
                         consumirBK anidado cl cont s = case s of
                                                              ('-':('-':cs)) -> consumirBK anidado cl cont $ dropWhile ((/=) '\n') cs
                                                              ('{':('-':cs)) -> consumirBK (anidado+1) cl cont cs
                                                              ('-':('}':cs)) -> case anidado of
                                                                                 0 -> \line -> lexer cont cs (line+cl)
                                                                                 _ -> consumirBK (anidado-1) cl cont cs
                                                              ('\n':cs) -> consumirBK anidado (cl+1) cont cs
                                                              (_:cs) -> consumirBK anidado cl cont cs

list s = parseList s 1
func s = parseFunc s 1
parseComm s = parse_Comm s 1

}
